## 01-03-2024


Billeder af 4 moduler (Knap, LDR, Potentiometer, og RGB-LED) som færdig loddet prototype.

![Status loddet moduler](/attachments/IMG_20240301_103751479.jpg){width=50%}
![Status loddet moduler](/attachments/IMG_20240301_103810336.jpg){width=50%}

Billede af prototype standoff til moduler.

![Status standoff](/attachments/IMG_20240301_122131667.jpg){width=50%}
